## Prerequisites
* Java 11

## Running  tests
### Unit tests
Execute the following command in the root directory of the project
```
./mvnw clean test
```

### Integration tests
Execute the following command to run the integraion tests 
```
./mvnw clean verify -PintegrationTests
```

## Assumptions
* All string properties such as `client_number`, `desciption` are at most 255 characters
* The total sum of all transaction amounts can fit in `BigInteger`. 
The total count of all transaction can fit in `long`
* Description has a default value, and it is not changed very rarely.
Which is why it is the same value for 90% of the transaction.
* Split transaction divides the amount of an existing transaction,
 and creates a new transaction with the option to change description and type. 

