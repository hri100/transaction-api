create sequence hibernate_sequence start 1 increment 1

    create table transaction (
       id int8 not null,
        amount numeric(19, 2),
        client_number varchar(255) not null,
        creation_date timestamp not null,
        currency varchar(255),
        description VARCHAR(255) DEFAULT 'DefaultDescription',
        last_updated_date timestamp,
        transaction_type_id int8 not null,
        primary key (id)
    )

    create table transaction_type (
       transaction_type_id int8 not null,
        deposit boolean,
        refundable boolean,
        summary varchar(255),
        primary key (transaction_type_id)
    )

    alter table if exists transaction 
       add constraint FKnl0vpl01y6vu03hkpi4xupugo 
       foreign key (transaction_type_id) 
       references transaction_type
