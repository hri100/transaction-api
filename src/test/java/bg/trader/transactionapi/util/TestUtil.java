package bg.trader.transactionapi.util;

import bg.trader.transactionapi.model.Currency;
import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.model.TransactionType;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.fail;

public class TestUtil {

    public static List<Transaction> createTransactions(List<TransactionType> types) {
        List<Transaction> transaction = Stream.of("clientNumber1", "clientNumber1", "clientNumber2",
                "clientNumber3", "clientNumber4", "clientNumber5",
                "clientNumber6", "clientNumber6", "clientNumber7")
                .map(TestUtil::createTransactionWithClientNumber).collect(Collectors.toList());
        for (int i = 0; i < 9; i++) {
            transaction.get(i).setType(types.get(i % types.size()));
        }

        return transaction;
    }


    public static List<TransactionType> createTransactionTypes() {
        List<TransactionType> types = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            types.add(createTransactionType());
        }
        return types;
    }


    public static Transaction createTransaction() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy");
        Transaction transaction = new Transaction();
        transaction.setClientNumber("testClientId");
        transaction.setCurrency(Currency.GBP);
        transaction.setDescription("testDescription");
        transaction.setAmount(new BigDecimal("10.00"));
        transaction.setCreationDate(format.parse("01-07-2020"));
        transaction.setLastUpdatedDate(format.parse("01-07-2020"));
        return transaction;
    }

    public static TransactionType createTransactionType() {
        TransactionType transactionType = new TransactionType();
        transactionType.setSummary("testSummary");
        transactionType.setDeposit(true);
        transactionType.setRefundable(true);

        return transactionType;
    }

    public static Transaction createTransactionWithClientNumber(String clientNumber) {
        Transaction transaction = new Transaction();
        transaction.setClientNumber(clientNumber);
        transaction.setAmount(new BigDecimal("10.00"));
        transaction.setCreationDate(new Date());

        return transaction;
    }

    public static Transaction createTransactionWithDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal("10.00"));
        transaction.setClientNumber("testClientNumber");
        try {
            transaction.setCreationDate(sdf.parse(date));
        } catch (ParseException e) {
            fail();
        }
        return transaction;
    }
}

