package bg.trader.transactionapi.controller;

import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.model.TransactionType;
import bg.trader.transactionapi.repository.TransactionRepository;
import bg.trader.transactionapi.repository.TransactionTypeRepository;
import bg.trader.transactionapi.request.data.SplitTransactionRequestData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static bg.trader.transactionapi.util.TestUtil.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class TransactionControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionTypeRepository typeRepository;

    private List<TransactionType> types;

    private List<Transaction> transactions;

    private static ObjectMapper objectMapper = new ObjectMapper();


    @BeforeEach
    public void setUp() {
        types = createTransactionTypes();
        typeRepository.saveAll(types);
        transactions = createTransactions(types);
        transactionRepository.saveAll(transactions);

    }

    @AfterEach
    public void tearDown() {
        transactionRepository.deleteAll();
        typeRepository.deleteAll();
    }

    @Test
    public void testPostTransaction() throws Exception {
        Transaction newTransaction = createTransaction();
        newTransaction.setDescription("testCustomDescription");
        newTransaction.setType(types.get(0));
        String requestData = objectMapper.writeValueAsString(newTransaction);

        MvcResult response = mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestData))
                .andExpect(status().isOk())
                .andReturn();

        Transaction responseTransaction = objectMapper.readValue(response.getResponse().getContentAsString(), Transaction.class);
        assertEquals(newTransaction.getDescription(), responseTransaction.getDescription());
        assertEquals(newTransaction.getAmount(), responseTransaction.getAmount());
        assertEquals(newTransaction.getCurrency(), responseTransaction.getCurrency());
    }

    @Test
    public void testDeleteTransaction() throws Exception {
        Transaction toDelete = transactions.get(0);
        transactions.remove(0);

        mockMvc.perform(delete("/transactions/{id}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Optional<Transaction> deleted = transactionRepository.findById(toDelete.getId());
        assertTrue(deleted.isEmpty());
    }

    @Test
    public void testUpdateTransaction() throws Exception {
        Transaction transaction = transactions.get(0);
        transactions.remove(0);
        BigDecimal updatedAmount = new BigDecimal("23.00");
        String updatedDescription = "updatedDescription";
        transaction.setAmount(updatedAmount);
        transaction.setDescription(updatedDescription);
        String requestData = objectMapper.writeValueAsString(transaction);

        mockMvc.perform(patch("/transactions/{id}", transaction.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestData))
                .andExpect(status().isOk())
                .andReturn();

        Optional<Transaction> updatedTransaction = transactionRepository.findById(transaction.getId());
        assertTrue(updatedTransaction.isPresent());
        assertEquals(updatedDescription, updatedTransaction.get().getDescription());
        assertEquals(updatedAmount, updatedTransaction.get().getAmount());

    }

    @Test
    public void testSplitTransaction() throws Exception {
        Transaction transaction = transactions.get(0);
        transactions.remove(0);

        SplitTransactionRequestData requestData = new SplitTransactionRequestData(
                transaction.getType().getTransactionTypeId(),
                "uniqueTransactionDescription");

        mockMvc.perform(post("/transactions/{id}/split", transaction.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(requestData)))
                .andExpect(status().isOk())
                .andReturn();

        Optional<Transaction> splitTransaction = transactionRepository.findById(transaction.getId());
        List<Transaction> dbTransactions = (List<Transaction>) transactionRepository.findAll();
        Optional<Transaction> newSplitTransaction = dbTransactions.stream().filter(s -> "uniqueTransactionDescription".equals(s.getDescription())).findFirst();
        assertTrue(splitTransaction.isPresent());
        assertEquals(new BigDecimal("5.00"), splitTransaction.get().getAmount());
        assertTrue(newSplitTransaction.isPresent());
        assertEquals(new BigDecimal("5.00"), newSplitTransaction.get().getAmount());

    }

    @Test
    public void getTransactionByClientNumberAndType() throws Exception {
        String id = Long.toString(types.get(0).getTransactionTypeId());
        MvcResult result = mockMvc.perform(get("/transactions/byclientnumberandtype")
                .param("clientNumber", "testClientId")
                .param("typeId", id))
                .andExpect(status().isOk())
                .andReturn();

        List<Transaction> responseTransactions = objectMapper.readValue(result.getResponse().getContentAsString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Transaction.class));
        assertEquals(transactions.stream()
                .filter(s -> s.getType().getTransactionTypeId() == types.get(0)
                        .getTransactionTypeId()).collect(Collectors.toList()), responseTransactions);
    }

    @Test
    public void getTransactionAmountSumByClientNumberAndType() throws Exception {
        String id = Long.toString(types.get(0).getTransactionTypeId());
        MvcResult result = mockMvc.perform(get("/transactions/amount/byclientnumberandtype")
                .param("clientNumber", "testClientId")
                .param("typeId", id))
                .andExpect(status().isOk())
                .andReturn();

        BigDecimal sum = new BigDecimal("0.0");
        for (Transaction t : transactions) {
            if (t.getType().getTransactionTypeId() == types.get(0).getTransactionTypeId())
                sum = sum.add(t.getAmount());
        }

        String response = result.getResponse().getContentAsString();
        assertEquals(sum, new BigDecimal(response));
    }

    @Test
    public void getTransactionCountByClientNumberAndCreationDate() throws Exception {
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(types.get(0));
                    transactionRepository.save(t);
                });


        MvcResult result = mockMvc.perform(get("/transactions/byclientnumberandcreationdate/count")
                .param("clientNumber", "testClientNumber")
                .param("start", "24-07-2020")
                .param("end", "01-09-2020"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("2", result.getResponse().getContentAsString());
    }

    @Test
    public void getTransactionsByClientNumberAndCreationDate() throws Exception {
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(types.get(0));
                    transactionRepository.save(t);
                });

        MvcResult result = mockMvc.perform(get("/transactions/byclientnumberandcreationdate")
                .param("clientNumber", "testClientNumber")
                .param("start", "24-07-2020")
                .param("end", "01-09-2020"))
                .andExpect(status().isOk())
                .andReturn();

        List<Transaction> responseTransactions = objectMapper.readValue(result.getResponse().getContentAsString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Transaction.class));
        assertEquals(2, responseTransactions.size());
    }

    @Test
    public void getTransactionAmountSumByClientNumberAndCreationDate() throws Exception {
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(types.get(0));
                    transactionRepository.save(t);
                });

        MvcResult result = mockMvc.perform(get("/transactions/amount/byclientnumberandcreationdate/sum")
                .param("clientNumber", "testClientNumber")
                .param("start", "24-07-2020")
                .param("end", "01-09-2020"))
                .andExpect(status().isOk())
                .andReturn();
        String response = result.getResponse().getContentAsString();
        assertEquals(new BigDecimal("20.00"), new BigDecimal(response));
    }

    @Test
    public void testGetTransactionsByClientList() throws Exception {
        MvcResult result = mockMvc.perform(get("/transactions/byclientnumberlist")
                .param("clientNumber", "clientNumber1")
                .param("clientNumber", "clientNumber6"))
                .andExpect(status().isOk())
                .andReturn();

        List<Transaction> responseTransactions = objectMapper.readValue(result.getResponse().getContentAsString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Transaction.class));
        assertEquals(4, responseTransactions.size());
    }

    @Test
    public void testGetTransactionsByTypeOrDateCreatedBetween() throws Exception {
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(types.get(0));
                    transactionRepository.save(t);
                });

        MvcResult result = mockMvc.perform(get("/transactions/bytypenumberandcreationdate")
                .param("typeId", Long.toString(types.get(0).getTransactionTypeId()))
                .param("start", "24-07-2020")
                .param("end", "01-09-2020"))
                .andExpect(status().isOk())
                .andReturn();

        List<Transaction> responseTransactions = objectMapper
                .readValue(result.getResponse().getContentAsString(),
                        objectMapper.getTypeFactory().constructCollectionType(List.class, Transaction.class));

        assertEquals(2, responseTransactions.size());
    }

    @Test
    public void testTransactionCountByTypeAndCreationDAteBetween() throws Exception {
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(types.get(0));
                    transactionRepository.save(t);
                });

        MvcResult result = mockMvc.perform(get("/transactions/bytypenumberandcreationdate/count")
                .param("typeId", Long.toString(types.get(0).getTransactionTypeId()))
                .param("start", "24-07-2020")
                .param("end", "01-09-2020"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("2", result.getResponse().getContentAsString());
    }

}