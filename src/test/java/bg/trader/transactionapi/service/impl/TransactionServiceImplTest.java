package bg.trader.transactionapi.service.impl;

import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.model.TransactionType;
import bg.trader.transactionapi.repository.TransactionRepository;
import bg.trader.transactionapi.repository.TransactionTypeRepository;
import bg.trader.transactionapi.request.data.SplitTransactionRequestData;
import bg.trader.transactionapi.service.TransactionService;
import bg.trader.transactionapi.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static bg.trader.transactionapi.util.TestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TransactionServiceImplTest {

    private static final int PAGE = 0;
    private static final int PAGE_SIZE = 10;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionTypeRepository transactionTypeRepository;

    @AfterEach
    public void tearDown() {
        transactionRepository.deleteAll();
        transactionTypeRepository.deleteAll();
    }

    @Test
    public void testSaveTransaction() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        transaction.setType(type);

        transactionService.save(transaction);
        Optional<Transaction> dbTransaction = transactionRepository.findById(transaction.getId());

        assertTrue(dbTransaction.isPresent());
        assertEquals(transaction.getId(), dbTransaction.get().getId());
    }

    @Test
    public void testDeleteTransaction() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        transaction.setType(type);
        transactionRepository.save(transaction);
        Long deletedId = transactionService.delete(transaction.getId());
        Optional<Transaction> dbTransaction = transactionRepository.findById(deletedId);

        assertFalse(dbTransaction.isPresent());
    }

    @Test
    public void testUpdateTransactionWithAmountAndDescription() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        transaction.setType(type);
        transactionRepository.save(transaction);

        Transaction newAmountAndDescription = new Transaction();
        newAmountAndDescription.setDescription("updatedTestDescription");
        newAmountAndDescription.setAmount(new BigDecimal("10000.13"));

        Transaction updated = transactionService.updateAmountAndDescription(newAmountAndDescription, transaction.getId());

        assertNotEquals(transaction, updated);
        assertEquals(newAmountAndDescription.getAmount(), updated.getAmount());
        assertEquals(newAmountAndDescription.getDescription(), updated.getDescription());
    }

    @Test
    public void testUpdateTransactionWithAmountAndNullDescription() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        transaction.setType(type);
        transactionRepository.save(transaction);
        Transaction newAmountAndDescription = new Transaction();
        newAmountAndDescription.setAmount(new BigDecimal("10000.13"));

        transactionService.updateAmountAndDescription(newAmountAndDescription, transaction.getId());
        Optional<Transaction> updated = transactionRepository.findById(transaction.getId());

        assertTrue(updated.isPresent());
        assertNotEquals(transaction, updated.get());
        assertEquals(newAmountAndDescription.getAmount(), updated.get().getAmount());
        assertNull(updated.get().getDescription());
    }

    @Test
    public void testUpdateTransactionWithDescriptionAndNullAmount() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        transaction.setType(type);
        transactionRepository.save(transaction);

        Transaction newAmountAndDescription = new Transaction();
        newAmountAndDescription.setDescription("updatedTestDescription");

        transactionService.updateAmountAndDescription(newAmountAndDescription, transaction.getId());
        Optional<Transaction> updated = transactionRepository.findById(transaction.getId());

        assertTrue(updated.isPresent());
        assertNotEquals(transaction, updated.get());
        assertEquals(newAmountAndDescription.getDescription(), updated.get().getDescription());
        assertNull(updated.get().getAmount());
    }

    @Test
    public void testUpdateTransactionWithNullDescriptionAndNullAmount() throws ParseException {
        Transaction transaction = createTransaction();
        Transaction newAmountAndDescription = new Transaction();

        Exception e = assertThrows(IllegalArgumentException.class, () ->
                transactionService.updateAmountAndDescription(newAmountAndDescription, transaction.getId())
        );

        assertEquals("Failed updating transaction with amount: null and description: null", e.getMessage());
    }

    @Test
    public void testUpdateNonExistentTransactionWithAmountAndDescription() {
        Transaction newAmountAndDescription = new Transaction();
        newAmountAndDescription.setDescription("updatedTestDescription");

        Exception e = assertThrows(IllegalArgumentException.class,
                () -> transactionService.updateAmountAndDescription(newAmountAndDescription, 132l)
        );

        assertEquals("Failed retrieving transaction with id: 132 from the DB", e.getMessage());
    }

    @Test
    public void testSplitTransaction() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transaction.setType(type);
        transactionTypeRepository.save(type);
        transactionRepository.save(transaction);

        transactionService.split(transaction.getId(),
                new SplitTransactionRequestData(transaction.getType().getTransactionTypeId(), "newTestDescription"));

        List<Transaction> transactions = (List<Transaction>) transactionRepository.findAll();

        assertEquals(2, transactions.size());
        for (int i = 0; i < 2; i++) {
            assertEquals(transaction.getAmount().divide(new BigDecimal(2)), transactions.get(i).getAmount());
        }
    }

    @Test
    public void testSplitNonExistentTransaction() {
        Exception e = assertThrows(IllegalArgumentException.class, () ->
                transactionService.split(123l,
                        new SplitTransactionRequestData(1l, "newTestDescription2"))
        );

        assertEquals("Failed retrieving transaction with id: 123 from the DB", e.getMessage());
    }

    @Test
    public void testSplitTransactionWithNonExistentTypes() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        transaction.setType(type);
        transactionRepository.save(transaction);

        Exception e = assertThrows(IllegalArgumentException.class, () ->
                transactionService.split(transaction.getId(),
                        new SplitTransactionRequestData(123l, "newTestDescription"))
        );

        assertEquals("Failed retrieving transactionType with id:123 from the DB", e.getMessage());
    }

    @Test
    public void testGetTransactionAmountByClientNumberAndTransactionType() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transaction.setType(type);
        transactionTypeRepository.save(type);
        transactionRepository.save(transaction);

        BigDecimal actualAmountSum = transactionService.sumAmountByClientNumberAndType(transaction.getClientNumber(),
                transaction.getType().getTransactionTypeId());

        assertEquals(new BigDecimal("10.00"), actualAmountSum);
    }

    @Test
    public void testGetTransactionAmountByClientNumberAndNonExistentTransactionType() {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        Stream.of("clientNumber1", "clientNumber1", "clientNumber2",
                "clientNumber3", "clientNumber4", "clientNumber5")
                .forEach((clientNumber) -> {
                    Transaction t = createTransactionWithClientNumber(clientNumber);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        BigDecimal actualAmountSum = transactionService
                .sumAmountByClientNumberAndType("clientNumber1", 123l);

        assertNotEquals(123l, type.getTransactionTypeId());
        assertEquals(new BigDecimal("20.00"), actualAmountSum);
    }

    @Test
    public void testGetTransactionAmountByNonExistentClientNumberAndTransactionType() {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        Stream.of("clientNumber1", "clientNumber1", "clientNumber2",
                "clientNumber3", "clientNumber4", "clientNumber5")
                .forEach((clientNumber) -> {
                    Transaction t = createTransactionWithClientNumber(clientNumber);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        BigDecimal actualAmountSum = transactionService
                .sumAmountByClientNumberAndType("nonExistentClientNumber", type.getTransactionTypeId());

        assertEquals(new BigDecimal("60.00"), actualAmountSum);
    }

    @Test
    public void testGetTransactionAmountByNonExistentClientNumberAndNonExistentType() {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        Stream.of("clientNumber1", "clientNumber1", "clientNumber2",
                "clientNumber3", "clientNumber4", "clientNumber5")
                .forEach((clientNumber) -> {
                    Transaction t = createTransactionWithClientNumber(clientNumber);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        BigDecimal actualAmountSum = transactionService
                .sumAmountByClientNumberAndType("nonExistentClientNumber", 123l);

        assertNotEquals(123l, type.getTransactionTypeId());
        assertNull(actualAmountSum);
    }

    @Test
    public void testGetTransactionCountByDateBetweenAndTransactionType() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        long count = transactionService.getTransactionCountByTypeAndDateBetween(type.getTransactionTypeId(),
                sdf.parse("06-06-2020"), sdf.parse("09-09-2020"));

        assertEquals(2, count);
    }

    @Test
    public void testGetTransactionCountByDateBetweenAndNonExistentTransactionType() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        long testTypeId = 123l;
        long count = transactionService.getTransactionCountByTypeAndDateBetween(testTypeId,
                sdf.parse("06-06-2020"), sdf.parse("09-09-2020"));

        assertNotEquals(testTypeId, type.getTransactionTypeId());
        assertEquals(2, count);
    }

    @Test
    public void testGetTransactionsByClientNumberAndType() throws ParseException {
        Transaction transaction = createTransaction();
        TransactionType type = createTransactionType();
        transaction.setType(type);
        transactionTypeRepository.save(type);
        transactionRepository.save(transaction);

        List<Transaction> transactions = transactionService.getTransactionsByClientNumberAndType(transaction.getClientNumber(),
                type.getTransactionTypeId(), PageRequest.of(0, 10));

        assertEquals(1, transactions.size());
        assertEquals(transaction.getId(), transactions.get(0).getId());
    }

    @Test
    public void testGetTransactionsByClientNumberAndNullTransactionType() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        for (int i = 0; i < 3; i++) {
            Transaction transaction = createTransaction();
            transaction.setType(type);
            transactionRepository.save(transaction);
        }

        List<Transaction> transactions = transactionService.getTransactionsByClientNumberAndType(null,
                type.getTransactionTypeId(), PageRequest.of(0, 10));

        assertEquals(3, transactions.size());
    }

    @Test
    public void testGetTransactionCountByClientNumberAndCreationDate() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        long count = transactionService.getTransactionCountByClientNumberAndCreationDateBetween("testClientNumber",
                new SimpleDateFormat("dd-M-yyyy").parse("01-07-2020"),
                new SimpleDateFormat("dd-M-yyyy").parse("01-08-2020"));

        assertEquals(2, count);
    }

    @Test
    public void testGetTransactionCountByClientNumberAndCreationDateBetweenInvalidDates() throws ParseException {
        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach(TestUtil::createTransactionWithDate);

        Exception e = assertThrows(IllegalArgumentException.class, () ->
                transactionService.getTransactionCountByClientNumberAndCreationDateBetween("testClientNumber",
                        new SimpleDateFormat("dd-M-yyyy").parse("01-10-2020"),
                        new SimpleDateFormat("dd-M-yyyy").parse("01-08-2020"))
        );

        assertEquals("Failed to get transaction count start date is after end date" +
                " startDate: Thu Oct 01 00:00:00 EEST 2020, endDate: Sat Aug 01 00:00:00 EEST 2020", e.getMessage());
    }

    @Test
    public void testGetTransactionAmountByClientNumberAndCreationDate() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        BigDecimal amountSum = transactionService.getTransactionAmountByClientNumberAndCreationDateBetween("testClientNumber",
                new SimpleDateFormat("dd-M-yyyy").parse("01-07-2020"),
                new SimpleDateFormat("dd-M-yyyy").parse("01-08-2020"));

        assertEquals(new BigDecimal("20.00"), amountSum);
    }

    @Test
    public void testGetTransactionAmountByClientNumberAndCreationDateBetweenInvalidDates() {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        Exception e = assertThrows(IllegalArgumentException.class, () ->
                transactionService.getTransactionAmountByClientNumberAndCreationDateBetween("testClientNumber",
                        new SimpleDateFormat("dd-M-yyyy").parse("01-10-2020"),
                        new SimpleDateFormat("dd-M-yyyy").parse("01-08-2020"))
        );

        assertEquals("Failed to get transaction count start date is after end date" +
                " startDate: Thu Oct 01 00:00:00 EEST 2020, endDate: Sat Aug 01 00:00:00 EEST 2020", e.getMessage());
    }

    @Test
    public void testGetTransactionsByClientNumberAndDateBetween() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        List<Transaction> transactions = transactionService.
                getTransactionsByClientNumberAndDateBetween("testClientNumber",
                        new SimpleDateFormat("dd-M-yyyy").parse("01-06-2020"),
                        new SimpleDateFormat("dd-M-yyyy").parse("01-08-2020"),
                        PageRequest.of(PAGE, PAGE_SIZE));

        assertEquals(2, transactions.size());
    }

    @Test
    public void testGetTransactionsByClientNumberAndDateBetweenInvalidDates() {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        Exception e = assertThrows(IllegalArgumentException.class, () -> {
            transactionService.getTransactionsByClientNumberAndDateBetween("testClientNumber",
                    new SimpleDateFormat("dd-M-yyyy").parse("01-09-2020"),
                    new SimpleDateFormat("dd-M-yyyy").parse("01-08-2020"),
                    PageRequest.of(PAGE, PAGE_SIZE));
        });

        assertEquals("Failed to get transaction count start date is after end date " +
                "startDate: Tue Sep 01 00:00:00 EEST 2020, endDate: Sat Aug 01 00:00:00 EEST 2020", e.getMessage());
    }

    @Test
    public void testGetTransactionsByClientNumbers() {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);

        Stream.of("testClientNumber1", "testClientNumber1",
                "testClientNumber2", "testClientNumber3",
                "testClientNumber4", "testClientNumber5").
                forEach((clientNumber) -> {
                    Transaction t = createTransactionWithClientNumber(clientNumber);
                    t.setType(type);
                    transactionRepository.save(t);
                });
        List<Transaction> transactions = transactionService
                .getTransactionsByClientNumbers(List.of("testClientNumber1"), PageRequest.of(PAGE, PAGE_SIZE));

        assertEquals(2, transactions.size());
    }

    @Test
    public void testTransactionAmountSumByClientNumbersAndIncludingTypes() {
        TransactionType type1 = createTransactionType();
        TransactionType type2 = createTransactionType();

        transactionTypeRepository.saveAll(Stream.of(type1, type2).collect(Collectors.toList()));

        List<String> clientNumbers = new LinkedList<>();
        Stream.of("clientNumber1", "clientNumber1", "clientNumber2",
                "clientNumber3", "clientNumber4", "clientNumber5")
                .forEach((clientNumber) -> {
                    Transaction t = createTransactionWithClientNumber(clientNumber);
                    TransactionType type = clientNumber.contains("1") || clientNumber.contains("5")
                            ? type1 : type2;
                    t.setType(type);
                    transactionRepository.save(t);
                    clientNumbers.add(clientNumber);
                });

        BigDecimal amountSum = transactionService.getTransactionAmountByClientNumbersAndIncludingTypes(clientNumbers,
                List.of(type1.getTransactionTypeId()));

        assertEquals(amountSum, new BigDecimal("30.00"));

    }

    @Test
    public void testTransactionAmountSumByClientNumbersAndExcludedTypes() {
        TransactionType type1 = createTransactionType();
        TransactionType type2 = createTransactionType();

        transactionTypeRepository.saveAll(Stream.of(type1, type2).collect(Collectors.toList()));

        List<String> clientNumbers = new LinkedList<>();
        Stream.of("clientNumber1", "clientNumber1", "clientNumber2",
                "clientNumber3", "clientNumber4", "clientNumber5")
                .forEach((clientNumber) -> {
                    Transaction t = createTransactionWithClientNumber(clientNumber);
                    TransactionType type = clientNumber.contains("1") || clientNumber.contains("5")
                            ? type1 : type2;
                    t.setType(type);
                    transactionRepository.save(t);
                    clientNumbers.add(clientNumber);
                });

        BigDecimal amountSum = transactionService.getTransactionAmountByClientNumbersAndExcludedTypes(clientNumbers,
                List.of(type1.getTransactionTypeId()));

        assertEquals(amountSum, new BigDecimal("30.00"));

    }

    @Test
    public void testGetTransactionsByTypeOrCreationDateBetween() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        List<Transaction> transactions = transactionService
                .getTransactionsByTypeAndCreationDateBetween(type.getTransactionTypeId(),
                        sdf.parse("01-07-2020"),
                        sdf.parse("01-09-2020"),
                        PageRequest.of(PAGE, PAGE_SIZE));

        assertEquals(2, transactions.size());
    }

    @Test
    public void testGetTransactionsByNonExistentTypeOrCreationDateBetween() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        long nonExistedTypeId = 1234l;
        List<Transaction> transactions = transactionService
                .getTransactionsByTypeAndCreationDateBetween(nonExistedTypeId,
                        sdf.parse("01-07-2020"),
                        sdf.parse("01-09-2020"),
                        PageRequest.of(PAGE, PAGE_SIZE));

        assertNotEquals(nonExistedTypeId, type.getTransactionTypeId());
        assertEquals(2, transactions.size());
    }

    @Test
    public void testGetTransactionByNonExistentTypeOrInvalidCreationDate() throws ParseException {
        TransactionType type = createTransactionType();
        transactionTypeRepository.save(type);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");

        Stream.of("31-07-2020", "29-07-2020", "01-10-2020")
                .forEach((date) -> {
                    Transaction t = createTransactionWithDate(date);
                    t.setType(type);
                    transactionRepository.save(t);
                });

        long nonExistedTypeId = 1234l;
        Exception e = assertThrows(IllegalArgumentException.class, () -> {
            transactionService
                    .getTransactionsByTypeAndCreationDateBetween(nonExistedTypeId,
                            sdf.parse("01-011-2020"),
                            sdf.parse("01-09-2020"),
                            PageRequest.of(PAGE, PAGE_SIZE));
        });

        assertEquals("Failed to get transaction count start date is after end date " +
                "startDate: Sun Nov 01 00:00:00 EET 2020, endDate: Tue Sep 01 00:00:00 EEST 2020", e.getMessage());

    }

}