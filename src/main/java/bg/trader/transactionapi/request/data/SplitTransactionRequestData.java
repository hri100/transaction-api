package bg.trader.transactionapi.request.data;

import java.util.Objects;

public class SplitTransactionRequestData {
    private long transactionType;
    private String transactionDescription;

    public SplitTransactionRequestData(long firstTransactionTypeId, String transactionDescription) {
        this.transactionType = firstTransactionTypeId;
        this.transactionDescription = transactionDescription;
    }

    public long getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(long transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SplitTransactionRequestData that = (SplitTransactionRequestData) o;
        return transactionType == that.transactionType &&
                Objects.equals(transactionDescription, that.transactionDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionType, transactionDescription);
    }

    @Override
    public String toString() {
        return "SplitTransactionRequestData{" +
                "transactionType=" + transactionType +
                ", transactionDescription='" + transactionDescription + '\'' +
                '}';
    }
}

