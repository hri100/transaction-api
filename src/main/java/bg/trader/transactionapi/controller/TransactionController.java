package bg.trader.transactionapi.controller;

import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.request.data.SplitTransactionRequestData;
import bg.trader.transactionapi.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static java.lang.String.format;

@Controller
public class TransactionController {
    private static final String DEFAULT_PAGE = "0";
    private static final String MAX_PAGE_NUMBER = "1000";
    private TransactionService transactionService;

    @Autowired
    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping(value = "/transactions", produces = "application/json")
    public ResponseEntity<Transaction> createTransaction(@RequestBody Transaction transaction) {

        Transaction savedTransaction = transactionService.save(transaction);
        return new ResponseEntity<Transaction>(savedTransaction, HttpStatus.OK);
    }

    @DeleteMapping(value = "/transactions/{id}", produces = "application/json")
    public ResponseEntity<Long> deleteTransaction(@PathVariable Long id) {
        if (id == null) {
            throw new IllegalArgumentException(format("Invalid id: %d", id));
        }

        Long deletedID = transactionService.delete(id);
        return new ResponseEntity<>(deletedID, HttpStatus.OK);
    }

    @PatchMapping(value = "/transactions/{id}", produces = "application/json")
    public ResponseEntity<Transaction> updateTransaction(@PathVariable Long id, @RequestBody Transaction transaction) {
        if (id == null || transaction == null || transaction.getAmount() == null || transaction.getDescription() == null) {
            throw new IllegalArgumentException(format("Invalid input data: id: %d, transaction: %s", id, transaction));
        }
        Transaction result = transactionService.updateAmountAndDescription(transaction, id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping(value = "/transactions/{id}/split")
    public void splitTransaction(@PathVariable Long id, @RequestBody SplitTransactionRequestData transactionData) {
        if (id == null || transactionData == null || transactionData.getTransactionDescription() == null) {
            throw new IllegalArgumentException(format("Invalid input data id: %d, transactionData: %s", id, transactionData));
        }
        transactionService.split(id, transactionData);
    }

    @GetMapping(value = "/transactions/byclientnumberandtype", produces = "application/json")
    public ResponseEntity<List<Transaction>> getTransactionsByClientIdAndType(@RequestParam(value = "clientNumber") String clientNumber,
                                                                              @RequestParam(value = "typeId") Long typeId,
                                                                              @RequestParam(value = "limit", defaultValue = MAX_PAGE_NUMBER) int limit,
                                                                              @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page) {

        if (clientNumber == null || typeId == null) {
            throw new IllegalArgumentException(format("Invalid input clientNumber: %s, typeId: %d", clientNumber, typeId));
        }

        List<Transaction> transactions = transactionService
                .getTransactionsByClientNumberAndType(clientNumber, typeId, PageRequest.of(page, limit));

        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/amount/byclientnumberandtype", produces = "application/json")
    public ResponseEntity<BigDecimal> getTransactionAmountByClientNumberAndType(
            @RequestParam(value = "clientNumber") String clientNumber,
            @RequestParam(value = "typeId") Long typeId,
            @RequestParam(value = "limit", defaultValue = MAX_PAGE_NUMBER) int limit,
            @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page) {

        if (clientNumber == null || typeId == null) {
            throw new IllegalArgumentException(format("Invalid input clientNumber: %s, typeId: %d", clientNumber, typeId));
        }

        BigDecimal result = transactionService.sumAmountByClientNumberAndType(clientNumber, typeId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/byclientnumberandcreationdate/count")
    public ResponseEntity<Long> getCountByClientNumberAndCreationDateBetween(@RequestParam(value = "clientNumber") String clientNumber,
                                                                             @RequestParam(value = "start") @DateTimeFormat(pattern = "dd-M-yyyy") Date start,
                                                                             @RequestParam(value = "end") @DateTimeFormat(pattern = "dd-M-yyyy") Date end) {

        if (clientNumber == null || start == null || end == null) {
            throw new IllegalArgumentException(format("Invalid input clientNumber: %s, startDate: %s, endDate: %s",
                    clientNumber, start, end));
        }

        long count = transactionService.getTransactionCountByClientNumberAndCreationDateBetween(clientNumber, start, end);
        return new ResponseEntity<Long>(count, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/byclientnumberandcreationdate", produces = "application/json")
    public ResponseEntity<List<Transaction>> getTransactionsByClientNumberAndCreationDateBetween(@RequestParam(value = "clientNumber") String clientNumber,
                                                                                                 @RequestParam(value = "start") @DateTimeFormat(pattern = "dd-M-yyyy") Date start,
                                                                                                 @RequestParam(value = "end") @DateTimeFormat(pattern = "dd-M-yyyy") Date end,
                                                                                                 @RequestParam(value = "limit", defaultValue = MAX_PAGE_NUMBER) int limit,
                                                                                                 @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page) {
        if (clientNumber == null || start == null || end == null) {
            throw new IllegalArgumentException(format("Invalid input clientNumber: %s, startDate: %s, endDate: %s",
                    clientNumber, start, end));
        }

        List<Transaction> transactions = transactionService
                .getTransactionsByClientNumberAndDateBetween(clientNumber, start, end, PageRequest.of(page, limit));
        return new ResponseEntity<List<Transaction>>(transactions, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/amount/byclientnumberandcreationdate/sum", produces = "application/json")
    public ResponseEntity<BigDecimal> getAmountSumByClientNumberAndDateBetween(@RequestParam(value = "clientNumber") String clientNumber,
                                                                               @RequestParam(value = "start") @DateTimeFormat(pattern = "dd-M-yyyy") Date start,
                                                                               @RequestParam(value = "end") @DateTimeFormat(pattern = "dd-M-yyyy") Date end) {
        if (clientNumber == null || start == null || end == null) {
            throw new IllegalArgumentException(format("Invalid input clientNumber: %s, startDate: %s, endDate: %s",
                    clientNumber, start, end));
        }

        BigDecimal sum = transactionService.getTransactionAmountByClientNumberAndCreationDateBetween(clientNumber,start,end);
        return new ResponseEntity<>(sum, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/byclientnumberlist", produces = "application/json")
    public ResponseEntity<List<Transaction>> getTransactionsByClientNumberList(@RequestParam(value = "clientNumber") List<String> clientNumbers,
                                                                               @RequestParam(value = "limit", defaultValue = MAX_PAGE_NUMBER) int limit,
                                                                               @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page) {
        if (clientNumbers == null) {
            throw new IllegalArgumentException(format("Invalid clientNumbers: %s", clientNumbers));
        }
        List<Transaction> transactions = transactionService.getTransactionsByClientNumbers(clientNumbers, PageRequest.of(page, limit));
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/bytypenumberandcreationdate", produces = "application/json")
    public ResponseEntity<List<Transaction>> getTransactionsByTypeAndDateBetween(@RequestParam(value = "typeId") String typeId,
                                                                                 @RequestParam(value = "start") @DateTimeFormat(pattern = "dd-M-yyyy") Date start,
                                                                                 @RequestParam(value = "end") @DateTimeFormat(pattern = "dd-M-yyyy") Date end,
                                                                                 @RequestParam(value = "limit", defaultValue = MAX_PAGE_NUMBER) int limit,
                                                                                 @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page) {
        if (typeId == null || start == null || end == null) {
            throw new IllegalArgumentException(format("Invalid input typeId: %s, startDate: %s, endDate: %s", typeId, start,end));
        }

        List<Transaction> transactions = transactionService
                .getTransactionsByTypeAndCreationDateBetween(Long.parseLong(typeId), start, end,PageRequest.of(page, limit));
        return new ResponseEntity<>(transactions, HttpStatus.OK);
    }

    @GetMapping(value = "/transactions/bytypenumberandcreationdate/count", produces = "application/json")
    public ResponseEntity<Long> getTransactionCountByTypeAndDateBetween(@RequestParam(value = "typeId") String typeId,
                                                                        @RequestParam(value = "start") @DateTimeFormat(pattern = "dd-M-yyyy") Date start,
                                                                        @RequestParam(value = "end") @DateTimeFormat(pattern = "dd-M-yyyy") Date end) {
        if (typeId == null || start == null || end == null) {
            throw new IllegalArgumentException(format("Invalid input typeId: %s, startDate: %s, endDate: %s", typeId, start,end));
        }

        long count = transactionService
                .getTransactionCountByTypeAndDateBetween(Long.parseLong(typeId), start, end);

        return new ResponseEntity<>(count, HttpStatus.OK);
    }
}
