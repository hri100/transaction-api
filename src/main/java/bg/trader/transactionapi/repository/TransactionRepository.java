package bg.trader.transactionapi.repository;

import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.model.TransactionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.net.ContentHandler;
import java.util.Date;
import java.util.List;

@Repository
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {
    @Query("SELECT SUM(t.amount) FROM Transaction t WHERE (t.clientNumber = ?1 AND type=?2) OR t.clientNumber = ?1 OR t.type = ?2 ")
    BigDecimal getTransactionSumByClientNumberAndType(String clientNumber, TransactionType type);

    @Query("SELECT COUNT(*) FROM Transaction t WHERE t.clientNumber = ?1 and t.creationDate BETWEEN ?2 and ?3 ")
    Long getTransactionCountByClientNumberAndCreationDateBetween(String clientNumber, Date start, Date end);

    @Query("SELECT SUM(t.amount) FROM Transaction t WHERE t.clientNumber = ?1 and t.creationDate BETWEEN ?2 and ?3 ")
    BigDecimal getTransactionAmountByClientNumberAndCreationDateBetween(String clientNumber, Date start, Date end);

    @Query("SELECT SUM(t.amount) FROM Transaction t WHERE t.clientNumber in ?1 AND t.type.transactionTypeId IN ?2")
    BigDecimal getTransactionAmountByClientNumbersAndIncludingTypes(List<String> clientNumbers, List<Long> types);

    @Query("SELECT SUM(t.amount) FROM Transaction t WHERE t.clientNumber in ?1 AND t.type.transactionTypeId NOT IN ?2")
    BigDecimal getTransactionAmountByClientNumbersAndExcludedTypes(List<String> clientNumbers, List<Long> excludedTypes);

    @Query("SELECT COUNT(*) FROM Transaction t WHERE (t.type = ?3 AND t.creationDate BETWEEN ?1 AND ?2) OR t.creationDate BETWEEN ?1 AND ?2")
    long getTransactionCountByTypeAndCreationDateBetween(Date start, Date end, TransactionType type);

    @Query("SELECT t FROM Transaction t WHERE (t.clientNumber = ?1 AND type= ?2) OR t.clientNumber = ?1 OR type = ?2")
    Page<Transaction> getTransactionsByClientNumberAndType(String clientNumber, TransactionType type, Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE t.clientNumber = ?1 AND t.creationDate BETWEEN ?2 AND ?3")
    Page<Transaction> getTransactionsByClientNumberAndDateBetween(String clientNumber, Date start, Date end, Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE t.clientNumber in ?1")
    Page<Transaction> getTransactionsByClientNumbers(List<String> clientNumbers, Pageable page);

    @Query("SELECT t FROM Transaction t WHERE (t.type = ?1 AND t.creationDate BETWEEN ?2 AND ?3) OR t.creationDate BETWEEN ?2 AND ?3")
    Page getTransactionsByTypeAndCreationDateBetween(TransactionType type, Date start, Date end, Pageable page);
}
