package bg.trader.transactionapi.repository;

import bg.trader.transactionapi.model.TransactionType;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransactionTypeRepository extends PagingAndSortingRepository<TransactionType, Long> {
}
