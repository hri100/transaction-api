package bg.trader.transactionapi.service.impl;

import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.model.TransactionType;
import bg.trader.transactionapi.repository.TransactionRepository;
import bg.trader.transactionapi.repository.TransactionTypeRepository;
import bg.trader.transactionapi.request.data.SplitTransactionRequestData;
import bg.trader.transactionapi.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static org.springframework.util.StringUtils.isEmpty;

@Service
public class TransactionServiceImpl implements TransactionService {
    public static final String DIVIDER = "2";
    private TransactionRepository transactionRepository;
    private TransactionTypeRepository transactionTypeRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  TransactionTypeRepository transactionTypeRepository) {
        this.transactionRepository = transactionRepository;
        this.transactionTypeRepository = transactionTypeRepository;
    }

    @Override
    public Transaction save(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public Long delete(Long id) {
        transactionRepository.deleteById(id);
        return id;
    }


    @Override
    public Transaction updateAmountAndDescription(Transaction transaction, long id) {
        validateAmountAndDescription(transaction);
        Transaction toUpdate = findById(id);
        toUpdate.setAmount(transaction.getAmount());
        toUpdate.setDescription(transaction.getDescription());
        save(toUpdate);
        return toUpdate;
    }

    private void validateAmountAndDescription(Transaction transaction) {
        if (transaction.getAmount() == null && isEmpty(transaction.getDescription())) {
            throw new IllegalArgumentException(
                    format("Failed updating transaction with amount: %s and description: %s",
                            transaction.getAmount(), transaction.getDescription()));
        }
    }

    @Override
    @Transactional
    public void split(long id, SplitTransactionRequestData requestData) {
        Transaction transaction = findById(id);
        transaction.setAmount(transaction.getAmount().divide(new BigDecimal(DIVIDER)));
        Transaction newTransaction = createTransactionFrom(transaction, requestData);

        save(transaction);
        save(newTransaction);
    }

    private Transaction findById(long id) {
        Optional<Transaction> toUpdate = transactionRepository.findById(id);
        if (toUpdate.isEmpty()) {
            throw new IllegalArgumentException(format("Failed retrieving transaction with id: %d from the DB", id));
        }
        return toUpdate.get();
    }

    private Transaction createTransactionFrom(Transaction transaction, SplitTransactionRequestData requestData) {
        Transaction newTransaction = new Transaction();
        newTransaction.setClientNumber(transaction.getClientNumber());
        newTransaction.setDescription(requestData.getTransactionDescription());
        newTransaction.setType(findByTypeId(requestData.getTransactionType()));
        newTransaction.setAmount(transaction.getAmount());
        newTransaction.setCreationDate(new Date());
        newTransaction.setLastUpdatedDate(new Date());

        return newTransaction;
    }

    private TransactionType findByTypeId(long typeId) {
        Optional<TransactionType> transactionType = transactionTypeRepository.findById(typeId);
        if (transactionType.isEmpty()) {
            throw new IllegalArgumentException(format("Failed retrieving transactionType with id:%d from the DB", typeId));
        }
        return transactionType.get();
    }

    @Override
    public BigDecimal sumAmountByClientNumberAndType(String clientNumber, Long typeId) {
        Optional<TransactionType> type = transactionTypeRepository.findById(typeId);
        return transactionRepository.getTransactionSumByClientNumberAndType(clientNumber, type.isEmpty() ? null : type.get());
    }

    @Override
    public Long getTransactionCountByClientNumberAndCreationDateBetween(String clientNumber, Date start, Date end) {
        validateDates(start, end);
        return transactionRepository.getTransactionCountByClientNumberAndCreationDateBetween(clientNumber, start, end);
    }

    @Override
    public BigDecimal getTransactionAmountByClientNumberAndCreationDateBetween(String clientNumber, Date start, Date end) {
        validateDates(start, end);
        return transactionRepository.getTransactionAmountByClientNumberAndCreationDateBetween(clientNumber, start, end);
    }

    @Override
    public BigDecimal getTransactionAmountByClientNumbersAndIncludingTypes(List<String> clientNumbers, List<Long> includedTypes) {
        return transactionRepository.getTransactionAmountByClientNumbersAndIncludingTypes(clientNumbers, includedTypes);
    }

    @Override
    public BigDecimal getTransactionAmountByClientNumbersAndExcludedTypes(List<String> clientNumbers, List<Long> excludedTypes) {
        return transactionRepository.getTransactionAmountByClientNumbersAndExcludedTypes(clientNumbers, excludedTypes);
    }

    @Override
    public long getTransactionCountByTypeAndDateBetween(Long typeId, Date start, Date end) {
        Optional<TransactionType> type = transactionTypeRepository.findById(typeId);
        return transactionRepository
                .getTransactionCountByTypeAndCreationDateBetween(start, end, type.isEmpty()? null : type.get());
    }

    @Override
    public List<Transaction> getTransactionsByClientNumberAndType(String clientNumber, Long typeId, Pageable page) {
        Optional<TransactionType> optionalType = transactionTypeRepository.findById(typeId);
        TransactionType type = optionalType.isEmpty() ? null : optionalType.get();
        return transactionRepository
                .getTransactionsByClientNumberAndType(clientNumber,
                        type, page).getContent();
    }

    @Override
    public List<Transaction> getTransactionsByClientNumberAndDateBetween(String clientNumber,
                                                                         Date start,
                                                                         Date end,
                                                                         Pageable page) {
        validateDates(start, end);
        return transactionRepository
                .getTransactionsByClientNumberAndDateBetween(clientNumber, start, end, page).getContent();
    }

    @Override
    public List<Transaction> getTransactionsByClientNumbers(List<String> clientNumbers, Pageable page) {
        return transactionRepository.getTransactionsByClientNumbers(clientNumbers, page).getContent();
    }

    @Override
    public List<Transaction> getTransactionsByTypeAndCreationDateBetween(long transactionTypeId,
                                                                         Date start, Date end,
                                                                         Pageable page) {
        validateDates(start, end);
        Optional<TransactionType> type = transactionTypeRepository.findById(transactionTypeId);
        TransactionType t = type.isEmpty() ? null : type.get();
        return transactionRepository.getTransactionsByTypeAndCreationDateBetween(t,start, end, page).getContent();
    }

    private void validateDates(Date start, Date end) {
        if (start.after(end)) {
            throw new IllegalArgumentException(format("Failed to get transaction count start date is after end date " +
                    "startDate: %s, endDate: %s", start, end));
        }
    }
}
