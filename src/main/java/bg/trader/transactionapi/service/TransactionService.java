package bg.trader.transactionapi.service;

import bg.trader.transactionapi.model.Transaction;
import bg.trader.transactionapi.request.data.SplitTransactionRequestData;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface TransactionService {

    Transaction save(Transaction transaction);
    Long delete(Long transaction);
    Transaction updateAmountAndDescription(Transaction transaction, long id);
    void split(long id, SplitTransactionRequestData requestData);
    BigDecimal sumAmountByClientNumberAndType(String clientNumber, Long typeId);
    Long getTransactionCountByClientNumberAndCreationDateBetween(String clientNumber, Date start, Date end);
    BigDecimal getTransactionAmountByClientNumberAndCreationDateBetween(String clientNumber, Date start, Date end);
    BigDecimal getTransactionAmountByClientNumbersAndIncludingTypes(List<String> clientNumbers, List<Long> includedTypes);
    BigDecimal getTransactionAmountByClientNumbersAndExcludedTypes(List<String> clientNumbers, List<Long> excludedTypes);
    long getTransactionCountByTypeAndDateBetween(Long typeId, Date start, Date end);
    List<Transaction> getTransactionsByClientNumberAndType(String clientNumber, Long typeId, Pageable page);
    List<Transaction> getTransactionsByClientNumberAndDateBetween(String clientNumber, Date start, Date end, Pageable page);
    List<Transaction> getTransactionsByClientNumbers(List<String> clientNumbers, Pageable page);
    List<Transaction> getTransactionsByTypeAndCreationDateBetween(long transactionTypeId, Date start, Date end, Pageable page);
}
