package bg.trader.transactionapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "transaction_type")
public class TransactionType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_type_id")
    private long transactionTypeId;
    @Column(name = "summary")
    private String summary;
    @OneToMany(mappedBy = "type")
    @JsonIgnore
    private List<Transaction> transactions;
    @Column(name = "refundable")
    private boolean isRefundable;
    @Column(name = "deposit")
    private boolean isDeposit;

    public long getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(long transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public boolean isRefundable() {
        return isRefundable;
    }

    public void setRefundable(boolean refundable) {
        isRefundable = refundable;
    }

    public boolean isDeposit() {
        return isDeposit;
    }

    public void setDeposit(boolean deposit) {
        isDeposit = deposit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionType that = (TransactionType) o;
        return transactionTypeId == that.transactionTypeId &&
                isRefundable == that.isRefundable &&
                isDeposit == that.isDeposit &&
                Objects.equals(summary, that.summary) &&
                Objects.equals(transactions, that.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionTypeId, summary, transactions, isRefundable, isDeposit);
    }

    @Override
    public String toString() {
        return "TransactionType{" +
                "transactionTypeId=" + transactionTypeId +
                ", summary='" + summary + '\'' +
                ", transactions=" + transactions +
                ", isRefundable=" + isRefundable +
                ", isDeposit=" + isDeposit +
                '}';
    }
}
